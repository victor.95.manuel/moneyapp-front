import './Login.css';
import GoogleUser from '../../google_client'
import React, {useState, useEffect} from 'react';
import {GoogleLogin} from 'react-google-login';
import {useHistory} from "react-router-dom";
import LoginRepository from './LoginRepository';
import {Base64} from 'js-base64';
import {toast, Toaster} from "react-hot-toast";
import {useIntl} from 'react-intl';
import {FormattedMessage} from 'react-intl';

export default (props) => {

    const HOME = '/home';
    const CREATE_USER = '/createUser'
    const LOGIN_SUCESS = 'DomainSuccess'
    const IMAGES_URL = '/statics/';
    const SPANISH_LOCALE = 'es';
    const ENGLISH_LOCALE = 'en';
    const history = useHistory();
    const intl = useIntl();
    const [currentUser, setCurrentUser] = useState('');
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    useEffect(() => {

        // window.gapi.load('auth2', () => {
        //     console.log('window.gapi init');
        //     window.gapi.auth2.init({
        //         client_id: GoogleUser.CLIENT_ID,
        //     }).then(() => {
        //         const currentOAuth = window.gapi.auth2.getAuthInstance();
        //         if (currentOAuth.isSignedIn.get()) loginSuccess();
        //     });
        // });

        // if (localStorage.getItem('isUserLogged') === '1') loginSuccess();

    }, [props.reload]);

    const responseGoogle = (response) => {
        console.log(response.profileObj);
    }

    const loginSuccess = () => {
        localStorage.setItem('isUserLogged', '1');
        localStorage.setItem('userLoggedName', username);
        toast.dismiss();
        history.push(HOME);
    }

    const createUser = () => {
        history.push(CREATE_USER);
    }

    const onUsernameChange = (username) => {
        setUsername(username.target.value)
    }

    const onPasswordChange = (password) => {
        setPassword(password.target.value);
    }

    const encodePassword = () => {
        return Base64.encode(password);
    }

    const cleanUsername = () => {
        document.getElementsByClassName('loginUsernameInput').item(0).value = null;
        document.getElementsByClassName('loginPasswordInput').item(0).value = null;
        setUsername('');
        setPassword('');
    }

    const showLoginUserError = () => {
        toast.error(intl.formatMessage({id: 'login.usernameError'}));
        document.getElementsByClassName('loginUsernameInput').item(0).style.boxShadow = "0 0 0 0.25rem red";
    }

    const updateErrors = (jsonToUpdate) => {
        if (jsonToUpdate === null) return;

        jsonToUpdate.map(
            (jsonElement) => {
                switch (jsonElement.element.className) {
                    case "User":
                        showLoginUserError();
                        cleanUsername();
                        break;
                    default:
                        break;
                }
            }
        );

        toast.error(intl.formatMessage({id: 'login.error'}));
    }

    const cleanErrors = () => {
        document.getElementsByClassName('loginUsernameInput').item(0).style.boxShadow = "none";
        document.getElementsByClassName('loginPasswordInput').item(0).style.boxShadow = "none";
    }

    const loginUser = () => {
        cleanErrors();

        const encodedPassword = encodePassword();

        LoginRepository()
            .login(username, encodedPassword)
            .then(response => {
                if (response[0].element.className !== LOGIN_SUCESS) updateErrors(response);
                else loginSuccess();
            });
    }

    const setLocaleToSpanish = () => {
        localStorage.setItem('currentLocaleLanguage', SPANISH_LOCALE);
        location.reload();
    }

    const setLocaleToEnglish = () => {
        localStorage.setItem('currentLocaleLanguage', ENGLISH_LOCALE);
        location.reload();
    }

    const renderTitleSection = () => {
        return <div className={'loginBody'}>
            <img className={'loginAppIcon'}
                // src={IMAGES_URL + 'moneyAppIcon.png'}
                 src={'./images/moneyAppIcon.png'}
                 alt="MealsApp Icon"/>
        </div>;
    }

    const renderLanguageSection = () => {
        return <table>
            <tbody>
            <tr>
                <td className={'loginRightTd'}>
                    <button className={'btn btn-outline-secondary loginLanguageSelection loginLeftLanguageButton'}
                            onClick={() => setLocaleToSpanish()}>
                        <img className={'loginLanguageButton'}
                            // src={IMAGES_URL + 'spanishIcon.png'}
                             src={'./images/spanishIcon.png'}
                             alt={'Spanish Icon'}/>
                    </button>
                </td>
                <td className={'loginLeftTd'}>
                    <button className={'btn btn-outline-secondary loginLanguageSelection loginRightLanguageButton'}
                            onClick={() => setLocaleToEnglish()}>
                        <img className={'loginLanguageButton'}
                            // src={IMAGES_URL + 'englishIcon.png'}
                             src={'./images/englishIcon.png'}
                             alt={'English Icon'}/>
                    </button>
                </td>
            </tr>
            </tbody>
        </table>;
    }

    const renderToastSection = () => {
        return <Toaster
            position="top-left"
            reverseOrder={false}
            toastOptions={
                {
                    style: {
                        fontSize: '1.4em',
                    }
                }
            }
        />;
    }

    function renderGoogleLoginSection() {
        return <GoogleLogin
            clientId={GoogleUser.CLIENT_ID}
            buttonText='Sign in'
            onSuccess={loginSuccess}
            onFailure={responseGoogle}
            cookiePolicy={'single_host_origin'}
            isSignedIn={true}
        />;
    }

    const renderLoginSection = () => {
        return <table>
            <tbody>
            <tr>
                <td className="loginTd"
                    colSpan='2'>
                    <input type={'text'}
                           className="form-control loginInput loginUsernameInput"
                           placeholder={intl.formatMessage({id: 'login.usernamePlaceholder'})}
                           onChange={onUsernameChange}/>
                </td>
            </tr>
            <tr>
                <td className="loginTd"
                    colSpan='2'>
                    <input type={'password'}
                           className="form-control loginInput loginPasswordInput"
                           placeholder={intl.formatMessage({id: 'login.passwordPlaceholder'})}
                           onChange={onPasswordChange}/>
                </td>
            </tr>
            <tr>
                <td className="loginCenterTd">
                    <button className="btn btn-outline-dark loginButton"
                            onClick={() => loginUser()}>
                        <p className="loginButtonText">
                            <FormattedMessage id='login.loginButton' defaultMessage='Sign in'/>
                        </p>
                    </button>
                </td>
                <td className="loginCenterTd">
                    <button className="btn btn-outline-dark loginButton"
                            onClick={() => createUser()}>
                        <p className="loginButtonText">
                            <FormattedMessage id='login.createUserButton' defaultMessage='Sign un'/>
                        </p>
                    </button>
                </td>
            </tr>
            <tr>
                <td className="loginCenterTd"
                    colSpan={'2'}>
                    <GoogleLogin
                        clientId={GoogleUser.CLIENT_ID}
                        buttonText={intl.formatMessage({id: 'login.loginButton'})}
                        onSuccess={loginSuccess}
                        onFailure={responseGoogle}
                        cookiePolicy={'single_host_origin'}
                        isSignedIn={true}
                    />
                </td>
            </tr>
            </tbody>
        </table>;
    }

    return (<React.Fragment>

            <div className={'container h-100'}>
                <div className="row h-100 justify-content-center">
                    <div className={'loginHeader'}/>
                    <div className="row justify-content-center align-items-end">
                        {renderTitleSection()}
                    </div>
                    <div className="row justify-content-center align-items-center">
                        {renderLanguageSection()}
                    </div>
                    <div className="row justify-content-center align-items-start">
                        {renderLoginSection()}
                    </div>
                    <div className={'loginFooter'}/>
                </div>
            </div>

            {renderToastSection()}

        </React.Fragment>
    );
}
