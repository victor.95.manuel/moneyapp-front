import ConstantURLs from '../../api_url';

export default function LoginRepository() {

    const login = (username, password) => {
        return fetch(ConstantURLs.API_URL_CURRENT + '/findUser?username=' + username + '&password=' + password)
            .then(response => response.json());
    }

    return {login}
}
