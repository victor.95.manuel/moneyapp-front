import ConstantURLs from '../../api_url'

export default function RegisterRepository() {

    const createUser = (userToSave) => {
        fetch(ConstantURLs.API_URL_CURRENT + '/createUser', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(userToSave),
        }).then();
    }

    return {createUser}
}
