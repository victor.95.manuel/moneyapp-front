import './Register.css';
import GoogleUser from '../../google_client'
import React, {useState, useEffect} from 'react';
import {GoogleLogin} from 'react-google-login';
import {useHistory} from "react-router-dom";
import RegisterRepository from './RegisterRepository';
import {Base64} from 'js-base64';

export default (props) => {

    const LOGIN = '/';
    const history = useHistory();
    const [usernameToSave, setUsernameToSave] = useState('');
    const [passwordToSave, setPasswordToSave] = useState('');
    const [repeatedPasswordToSave, setRepeatedPasswordToSave] = useState('');

    const encodePassword = () => {
        return Base64.encode(passwordToSave)
    }

    const createUser = () => {
        const passwordEncoded = encodePassword();

        const userToSave = {
            username: usernameToSave,
            password: passwordEncoded,
            provider: "LOCAL"
        }

        RegisterRepository()
            .createUser(userToSave);
    }

    const onUserNameChange = (username) => {
        setUsernameToSave(username.target.value);
    }

    const onPasswordChange = (password) => {
        setPasswordToSave(password.target.value);
    }

    const onRepeatedPasswordChange = (password) => {
        setRepeatedPasswordToSave(password.target.value);
    }

    const backToLogin = () => {
        history.push(LOGIN);
    }

    return (<React.Fragment>

            <div>
                <h2>SIGN UP</h2>
                <br/>
            </div>

            <label>
                Username:
            </label>

            <br/>

            <input type={'text'}
                   placeholder={'Username...'}
                   onChange={(username) => onUserNameChange(username)}/>

            <br/>
            <br/>

            <label>
                Password:
            </label>

            <br/>

            <input type={'password'}
                   placeholder={'Password...'}
                   onChange={(password) => onPasswordChange(password)}/>

            <br/>
            <br/>

            <label>
                Repeat Password:
            </label>

            <br/>

            <input type={'password'}
                   placeholder={'Password...'}
                   onChange={(password) => onRepeatedPasswordChange(password)}/>

            <br/>
            <br/>

            <button onClick={createUser}>
                Sign up
            </button>

            <br/>

            <button onClick={backToLogin}>
                Back
            </button>

        </React.Fragment>
    );
}
