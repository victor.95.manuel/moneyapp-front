import './Home.css';
import React, {useState, useEffect} from 'react';
import {useHistory} from "react-router-dom";
import HomeRepository from "./HomeRepository";
import {GoogleLogout} from 'react-google-login';

export default (props) => {

    const LOGIN = '/';
    const history = useHistory();

    const redirectToLogin = () => {
        history.push(LOGIN);
    }

    return (<React.Fragment>

            <div>
                <h2>HOME</h2>
                <br/>
            </div>

            <GoogleLogout
                clientId="56020826511-ivafgd5v1eel6cdbhtserggd98p9bkpj.apps.googleusercontent.com"
                buttonText="Logout"
                onLogoutSuccess={redirectToLogin}>
            </GoogleLogout>

        </React.Fragment>
    );
}
