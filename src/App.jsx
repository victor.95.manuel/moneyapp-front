import React from "react";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import './styles.css'
import Login from '../src/components/login/Login'
import Home from '../src/components/home/Home'
import Register from '../src/components/register/Register'

const App = (props) => {
    return (
        <Router>
            <Switch>
                <Route exact path="/">
                    <Login {...props}/>
                </Route>
                <Route exact path="/createUser">
                    <Register {...props}/>
                </Route>
                <Route exact path="/home">
                    <Home {...props}/>
                </Route>
            </Switch>
        </Router>
    );
};

export default App;
